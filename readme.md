# Glitch Effect Shader for Godot Engine 3.2 later

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/E1E44AWTA)

## Videos

- [Glitch Effect](https://www.youtube.com/watch?v=SS99LOzBCM4)
- [Glitch Transition](https://www.youtube.com/watch?v=llxXOjMuruw)

## License

MIT License

## Author

* @arlez80 あるる / きのもと 結衣 ( Yui Kinomoto )

## Video

https://www.youtube.com/watch?v=SS99LOzBCM4
